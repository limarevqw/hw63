<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Word;
use AppBundle\Form\WordFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class MainController
 * @package AppBundle\Controller
 */
class MainController extends Controller
{
    /**
     * @Route("/")
     * @Method({"GET", "HEAD", "POST"})
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $words = $this->getDoctrine()
            ->getRepository(Word::class)
            ->findAll();

        $formWord = $this->createForm(WordFormType::class);

        $word = new Word();

        $formWord->handleRequest($request);

        if ($formWord->isSubmitted() && $formWord->isValid()){
            $data = $formWord->getData();

            $word->translate('ru')->setText($data['text']);
            $em = $this->getDoctrine()->getManager();
            $em->persist($word);

            $word->mergeNewTranslations();

            $em->flush();

            return $this->redirectToRoute('app_main_index');
        }

        return $this->render("@App/main/index.html.twig", [
            'words' => $words,
            'formWord' => $formWord->createView()
        ]);
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/show/{word_id}")
     * @param Request $request
     * @param $word_id
     * @return Response
     */
    public function showAction(Request $request, $word_id)
    {

        $word = $this->getDoctrine()
            ->getRepository(Word::class)
            ->find($word_id);

        return $this->render("@App/main/show.html.twig", [
            'word' => $word
        ]);
    }

    /**
     * @Route("/{_locale}", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeLocal(Request $request)
    {
        return $this->redirect($request->headers->get('referer'));
    }


}
